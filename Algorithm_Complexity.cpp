/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/
#include <iostream>
#include <list>
#include <ctime>
#include <cmath>
#include <vector>
#include <algorithm>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;

void swap(int& l, int& r)
{
	int temp = l;
	l = r;
	r = temp;
}

void SelectionSort(char * input)
{
	ofstream ofile;
	int original = atoi(input);
	int swap_pos, minimum;
	float Averages[10], average = 0.0;
	string filename = "SelectionSort" + (string)input + "Scale.txt";
	ofile.open(filename.c_str());

	for(int r = 0; r < 10; r++)
	{
		int n = original + (r * original);

		ofile << "input size: " << n << endl;

		average = 0.0;

		for(int a = 0; a < 5; a++)
		{
			float total_time;
			vector<int> V;
			clock_t t;
			srand(time(NULL));

			cout << "Creating Vector with " << n << " elements..." << endl;

			for(int i = 0; i < n; i++) V.push_back(rand());

			cout << "Sorting..." << endl;

			t = clock();
			for(int c = 0; c < (n - 1); c++)
			{
				minimum = V[c];
				swap_pos = c;

				for(int k = c + 1; k < n; k++)
					if(minimum > V[k])
					{
						minimum = V[k];
						swap_pos = k;
					};

				swap(V[c], V[swap_pos]);
			}
			t = clock() - t;

			total_time = (float)t / CLOCKS_PER_SEC;

			average += total_time;

			cout << "Elapsed time is: " << total_time << endl
		   		 << "---------------------" << endl;

			ofile << total_time << endl;

		}

		ofile << "Average: " << average / 5.0 << endl << endl;
		Averages[r] = average / 5.0;
	}

	for(int c = 0; c < 10; c++)
		ofile << Averages[c] << endl;

	ofile.close();
}

void BinarySearchVector(char* input)
{
	ofstream ofile;
	int original = atoi(input);
	long double Averages[10], average = 0.0;
	string filename = "BinarySearch" + (string)input + "Scale.txt";
	ofile.open(filename.c_str());
	srand(time(NULL));

	for(int r = 0; r < 10; r++)
	{
		int n = original + (r * original);
		average = 0.0;

		ofile << "input size: " << n << endl;
		
		for(int a = 0; a < 10; a++)
		{
			vector<int> V;
			clock_t t;
			//int key = ((rand() * rand()) / rand()) % rand();
			//int key = 42;
			int key = rand();
			bool found;
			long double total_time;
			
			cout << "Creating a vector of " << n << " elements..." << endl;
			for(int i = 0; i < n; i++)
				V.push_back(i * i / 2);

			cout << "Sorting..." << endl;
			sort(V.begin(), V.end());

			cout << "Searching " << key << " on the vector..." << endl;
			
			
			t = clock();
			found = binary_search(V.begin(), V.end(), key);
			t = clock() - t;
			cout << t << endl;
			total_time = ((long double)t * 100)/ CLOCKS_PER_SEC;
			average+= total_time;

			if(found)
			{
				cout << "Found " << key << " in the vector of " << n
				 	 << " elements." << endl << "The elapsed time is: "
					 << total_time << endl
					 << "---------------------" << endl;

				ofile << "Found " << total_time << endl;
			}else
			{
				cout << "Didn't find " << key << " in the vector of " << n
					 << " elements." << endl << "The elapsed time is: "
					 << total_time << endl << "---------------------" << endl;

				ofile << "NotFound " << total_time << endl;
			}
		}

		ofile << "Average: " << average / 10.0 << endl << endl;
		Averages[r] = average / 10.0;
	}

	for(int c = 0; c < 10; c++)
		ofile << Averages[c] << endl;

	ofile.close();
}

void sortVector(char* input)
{
	ofstream ofile;
	int original = atoi(input);
	float Averages[10], average = 0.0;
	string filename = "sorting" + (string)input + "Scale.txt";
	ofile.open(filename.c_str());

	for(int k = 0; k < 10; k++)
	{
		int n = original + (k * original);
		average = 0.0;

		ofile << "input size: " << n << endl;

		for(int a = 0; a < 10; a++)
		{
			vector<int> V;
			clock_t t;
			srand(time(NULL));
			float total_time;

			cout << "Creating a vector of " << n << " elements..." << endl;
			for(int i = 0; i < n; i++) V.push_back(rand());

			cout << "Sorting..." << endl;
			t = clock();
			sort(V.begin(), V.end());
			t = clock() - t;
			total_time = (float)t / CLOCKS_PER_SEC;
			average += total_time;

			cout << "Elapsed time is: " << total_time << endl
		   		 << "---------------------" << endl;

			ofile << total_time << endl;
		}

		ofile << "Average: " << average / 10.0 << endl << endl;
		Averages[k] = average / 10.0;
	}

	for(int c = 0; c < 10; c++)
		ofile << Averages[c] << endl;

	ofile.close();
}

int main (int argc, char * input[]) {
    if (argc < 2) {
    	cout << "Usage: " << input[0] << " <input_size>\n";
    	return 1;
    }


	SelectionSort(input[1]);
	//BinarySearchVector(input[1]);
	//sortVector(input[1]);


    //cout << CLOCKS_PER_SEC << endl;

    return 0;
}
